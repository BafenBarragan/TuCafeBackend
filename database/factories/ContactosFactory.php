<?php

namespace Database\Factories;

use App\Models\contactos;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = contactos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
